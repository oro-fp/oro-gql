import { Db, Collection, ObjectID } from "mongodb";

export class Client {
    db: Db
    collection: Collection

    constructor(db:Db) {
        this.db = db;
        this.collection = db.collection("clients")
    }

    async client(args: any) {
        return await this.collection.findOne({"_id": new ObjectID(args.id)})
    }

    async clients() {
        return await this.collection.find().toArray()
    }

    async setClient(args: any) {
        var id = new ObjectID(args?._id || null)
        return await this.collection.findOneAndUpdate({_id: id}, {$set: args}, {upsert: true, }).then(async (resp) => {
            if(resp.value) {
                return resp.value
            } else {
                return await this.collection.findOne({"_id": id})
            }
        })
    }

}