import { Db, Collection, ObjectID } from "mongodb";

export class Activity {
    db: Db
    collection: Collection

    constructor(db:Db) {
        this.db = db;
        this.collection = db.collection("activities")
    }

    async activity(id: any) {
        return await this.collection.findOne({"_id": new ObjectID(id)})
    }

    async activities() {
        return await this.collection.find().toArray()
    }
}