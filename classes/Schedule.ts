import { Db, Collection, ObjectID } from "mongodb";
import {Activity} from "./Activity";

export class Schedule {
    db: Db
    collection: Collection

    constructor(db:Db) {
        this.db = db;
        this.collection = db.collection("schedules")
    }

    async schedule(args: any) {
        var schedule = await this.collection.findOne({"_id": new ObjectID(args.id)})
        return await this.collection.findOne({"_id": new ObjectID(args.id)})
    }

    async schedules(args: any) {
        if(args?.clientID){
            var clientID = new ObjectID(args?.clientID || null)
            return await this.collection.find({"clientID": clientID}).toArray()
        }
    }

    async setSchedule(args: any) {
        // Create ObjectID from id
        var id = new ObjectID(args?._id || null)
        return await this.collection.findOneAndUpdate({_id: id}, {$set: args}, {upsert: true, }).then(async (resp) => {
            if(resp.value) {
                return resp.value
            } else {
                return await this.collection.findOne({"_id": id})
            }
        })
    }
}