import { Db, Collection, ObjectID } from "mongodb";

export class Account {
    db: Db
    collection: Collection

    constructor(db:Db) {
        this.db = db;
        this.collection = db.collection("accounts")
    }

    async account(args: any) {
        if(args.email){
            return await this.collection.findOne({"email": args.email})
        }

        if(args.id){
            // console.log(await this.collection.findOne({"_id": new ObjectID(args.id)}))
            return await this.collection.findOne({"_id": new ObjectID(args.id)})
        }
    }
}