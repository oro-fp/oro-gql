import { MongoClient } from "mongodb";
import { loadFilesSync } from "@graphql-tools/load-files";
import { mergeTypeDefs, mergeResolvers} from "@graphql-tools/merge/es5";
const { ApolloServer } = require("apollo-server");
const path = require("path")

import { Account } from "./classes/Account"
import { Activity } from "./classes/Activity"
import { Client } from "./classes/Client"
import { Schedule } from "./classes/Schedule"


const typesArray = loadFilesSync('./typedefs', {extensions: ['graphql']});
const typeDefs = mergeTypeDefs(typesArray);

const resolversArray = loadFilesSync(path.join(__dirname, './resolvers'), {extensions: ['js', 'ts']});
const resolvers = mergeResolvers(resolversArray)



MongoClient.connect("mongodb://USERNAME:PASSWORD@HOST:27017/?authSource=admin", (err:any, mongoClient: MongoClient) =>{
    let oro_db = mongoClient.db("oro")

    let datasources =  {
        account: new Account(oro_db),
        activity: new Activity(oro_db),
        client: new Client(oro_db),
        schedule: new Schedule(oro_db),
    }

    const server = new ApolloServer({
        typeDefs: typeDefs,
        resolvers: resolvers,
        debug: true,
        dataSources: () => (datasources)
    });

    server.listen().then(async ({ url,  subscriptionsUrl }:any) => {
        console.log(`🚀  Server ready at ${url}`);
        console.log(`🚀  Server ready at ${subscriptionsUrl}`);
    });
});






