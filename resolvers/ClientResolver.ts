import {ObjectID} from "mongodb";

module.exports = {
    Query: {
        async client(source: any, args: any, {dataSources}: any){
            return await dataSources.client.client(args).then((results: any) => {
                return results
            })
        },
        async clients(source: any, args: any, {dataSources}: any){
            return await dataSources.client.clients(args).then((results: any) => {
                return results
            })
        },

        async setClient(source: any, args: any, {dataSources}: any){
            args._id = new ObjectID(args?._id)
            return await dataSources.client.setClient(args).then((results: any) => {
                return results
            })
        },
    },

    Mutation: {
        async setClient(parent: any, args: any, {dataSources}: any){
            return await dataSources.client.setClient(args).then((results: any) => {
                return results
            })
        },
    },

    Schedule: {
        async client(schedule: any, args: any, {dataSources}: any){
            return await dataSources.client.clients(schedule.clientID).then((results: any) => {
                return results
            })
        },
    }

}