module.exports = {
    Query: {
        async activity(source: any, args: any, {dataSources}: any){
            return await dataSources.activity.activity(args._id).then((results: any) => {
                return results
            })
        },
        async activities(source: any, args: any, {dataSources}: any){
            return await dataSources.activity.activities(args).then((results: any) => {
                return results
            })
        },
    },
    TimeTableItem: {
        async activity(timetableItem: any, args: any, {dataSources}: any){
            return await dataSources.activity.activity(timetableItem.activityID).then((results: any) => {
                return results
            })
        }
    }
}