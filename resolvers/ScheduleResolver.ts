import {ObjectID} from "mongodb";

module.exports = {
    Query: {
        async schedule(parent: any, args: any, {dataSources}: any){
            return await dataSources.schedule.schedule(args).then((results: any) => {
                return results
            })
        },

        async schedules(parent: any, args: any, {dataSources}: any){
            return await dataSources.schedule.schedules(args).then((results: any) => {
                return results
            })
        },
    },
    Mutation: {
        async setSchedule(parent: any, args: any, {dataSources}: any){
            // Create and replace client as ObjectID type
            args.clientID = new ObjectID(args.clientID)
            return await dataSources.schedule.setSchedule(args).then((results: any) => {
                return results
            })
        },
    },
    Schedule: {
        async item(schedule: any, args: any){
            // check if schedule and timeTables not null
            if(schedule !== null && schedule.timeTable !== null){
                // loop through schedule timetable
                for(var a in schedule.timeTable){
                    // check if index exists
                    if(schedule.timeTable.hasOwnProperty(a)){
                        // define property timetableItem
                        var timetableItem = schedule.timeTable[a];
                        // check if timetableitem equals argument time
                        if(timetableItem.time === args.time){
                            // return item
                            return timetableItem
                        }
                    }
                }
            }
        },
        async timeTable(schedule: any){
            // check if schedule and timeTables not null
            if(schedule !== null && schedule.timeTable !== null){
                // create timetable array
                var timeTable = []
                // loop through schedule timetable
                for(var a in schedule.timeTable){
                    // check if index exists
                    if(schedule.timeTable.hasOwnProperty(a)){
                        // define property timetableItem
                        var timetableItem = schedule.timeTable[a];
                        // push the timetableitem to the timetable array
                        timeTable.push(timetableItem)
                    }
                }
                // return the timetable
                return timeTable
            }
        },
        async setItem(schedule: any, args: any, {dataSources}: any){
            var index = 0;
            // check if timeTable exists
            if(schedule.timeTable == null){
                // set the timetable to the value of args if there is no timeTable
                schedule.timeTable = [args]
            } else {
                var itemFound = false;
                // loop through timeTable
                for(var tableItem in schedule.timeTable){
                    // Check if index exists
                    if(schedule.timeTable.hasOwnProperty(tableItem)){
                        // check if times match
                        if(schedule.timeTable[tableItem].time === args.time){
                            // set tableitem to value of args
                            schedule.timeTable[tableItem] = args
                            itemFound = true
                            index = parseInt(tableItem)
                        }
                    }
                }

                // if item hasn't been found, push to array
                if(!itemFound) {
                    schedule.timeTable.push(args)
                    index = (schedule.timeTable.length -1)
                }
            }

            // set schedule in DB
            await dataSources.schedule.setSchedule(schedule)
            return schedule.timeTable[index]
        },
        async delItem(schedule: any, args: any, {dataSources}: any){
            var deletedItem = null
            // make sure there is a timetable
            if(schedule.timeTable !== null){
                // loop through timetableitems
                for(var tableItem in schedule.timeTable){
                    // check if item exists
                    if(schedule.timeTable.hasOwnProperty(tableItem)) {
                        // check if time matches the parameter
                        if(schedule.timeTable[tableItem].time === args.time){
                            // save the deleted item for callback
                            deletedItem = schedule.timeTable[tableItem]
                            // remove the item from the timetable array
                            schedule.timeTable.splice(tableItem, 1)
                        }
                    }
                }
            }

            // update schedule in DB
            await dataSources.schedule.setSchedule(schedule)
            return deletedItem
        }
    },

    Client: {
        async schedules(client: any, args: any, {dataSources}: any){
            return await dataSources.schedule.schedules({clientID: client._id}).then((results: any) => {
                return results
            })
        },
    }
}